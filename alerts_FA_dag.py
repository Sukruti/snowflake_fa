import json
import sys
import requests

import airflow
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from whizpy.framework.fieldanalytics.py_files import config_fa

default_args = {
    'owner': 'Amulya',
    'depends_on_past': False,
    'start_date': airflow.utils.dates.days_ago(2),
    'email': config_fa.email,
    'email_on_failure': True,
    'email_on_retry': False

}

alerts_dag = DAG('Alerts_FA_Dag', default_args=default_args, catchup=False,
                 schedule_interval=config_fa.schedule,)


def startalertprocess():
    print("Start Alerting process")


def read_json():
    try:
        with open(config_fa.alert) as json_data:
            alert_json = json.load(json_data)
            print(json_data)
            print(type(alert_json))
            print("Opening json")
            alert_json_upd = change_date(alert_json)
            print(alert_json_upd['alertRequests'][0]['referenceDate'])
            print(type(alert_json_upd))
            json_data.close()
            return alert_json_upd
    except FileNotFoundError as e:
        print("Index File Not Found")
        print(e)
        sys.exit(1)


def change_date(alert_json):
    alert_json['alertRequests'][0]['referenceDate'] = config_fa.referenceDate
    return alert_json


def alerts():
    url = config_fa.url
    headers = {'content-type': 'application/json'}
    payload = read_json()
    try:
        response = requests.post(url, data=json.dumps(payload), headers=headers, verify=False)
        print(response.text)
        response.raise_for_status()
    except requests.exceptions.HTTPError as errh:
        print("Http Error:", errh)
        print(response.json())
    except requests.exceptions.ConnectionError as errc:
        print("Error Connecting:", errc)
        print(response.json())
    except requests.exceptions.Timeout as errt:
        print("Timeout Error:", errt)
        print(response.json())
    except requests.exceptions.RequestException as err:
        print("OOps: Something Else", err)
        print(response.json())


def end_process():
    print("Alerting process end")


startprocess = PythonOperator(
    task_id='start_alert_process',
    python_callable=startalertprocess,
    email=config_fa.email,
    email_on_failure=True,
    dag=alerts_dag
)

readfile = PythonOperator(
    task_id='read_json',
    python_callable=read_json,
    email=config_fa.email,
    email_on_failure=True,
    dag=alerts_dag
)

alert_user = PythonOperator(
    task_id='alert_user',
    python_callable=alerts,
    email=config_fa.email,
    email_on_failure=True,
    dag=alerts_dag
)

end_process = PythonOperator(
    task_id='end',
    python_callable=end_process,
    email=config_fa.email,
    email_on_failure=True,
    dag=alerts_dag
)

email = DummyOperator(task_id='send_email', dag=alerts_dag)

startprocess >> readfile >> alert_user >> end_process >> email