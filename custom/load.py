import logging
from datetime import datetime
# import pymsteams

logger = logging.getLogger(__name__)

def get_webhook_url():
    webhookURL = "https://whizai.webhook.office.com/webhookb2/4413ad95-3648-4f03-8b41-6bdbe831dc03@b1656a72-b5d5-4f8e-989c-b93362b92826/IncomingWebhook/38d0af507671498399518f07322e56f4/9954e76f-a3ca-47ce-bc13-fe1202593d1f"
    return webhookURL

def destination_data_validate(ctx_dict):
    logger.info("============= Custom destination_data_validate Logic ==============")
    return ctx_dict


def final_validation(ctx_dict):
    logger.info("============= Custom E2E Validation Logic ==============")
    return ctx_dict
