import logging

logger = logging.getLogger(__name__)


def nlp_postprocess(ctx_dict):
    logger.info("============= Custom NLP Postprocessing Logic ==============")
    return ctx_dict
