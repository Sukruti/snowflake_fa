import logging

logger = logging.getLogger(__name__)


def nlp_preprocess(ctx_dict):
    logger.info("============= Custom NLP Preprocessing Logic ==============")
    return ctx_dict
