import logging
from whizpy.framework.framework.context_keys import Context_Key as ck
logger = logging.getLogger(__name__)


def transform(ctx_dict):
    logger.info("============= Custom Transformation Logic ==============")
    #ctx_dict[ck.LOAD_DS_LIST.value] = ['speakerprogram', 'marketing','salesgoal']
    ctx_dict[ck.LOAD_DS_LIST.value] = ['sales']
    #### Framework will only load data for datasources in this list.
    return ctx_dict
