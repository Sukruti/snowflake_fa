import configparser

parser = configparser.RawConfigParser()
parser.read('dags/whizpy/framework/fieldanalytics/configurations/config_fa.ini')
alert = parser.get('filepath', 'alert')
configjson = parser.get('filepath', 'configfile')

# Schedule Interval
schedule = parser.get('scheduledinterval', 'schedule')
referenceDate = parser.get('scheduledinterval', 'referenceDate')

# Alerts
url = parser.get('alerts', 'url')

# Email Configuration
email = parser.get('email', 'emailid')